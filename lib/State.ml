module Make (S : sig
  type t
end) =
struct
  module Signature = struct
    type 'a t =
      | Get of (S.t -> 'a)
      | Set of S.t * (unit -> 'a)

    let map f s =
      match s with
      | Get h -> Get (fun s -> f (h s))
      | Set (x,h) -> Set (x,fun () -> f (h ()))
  end

  module FreeState = Free.Make (Signature)
  include FreeState

  (* Define [Signature] so that ['a FreeState.t] is isomorphic to the
          following type:

     <<<
          type 'a t =
            | Return of 'a
            | Get of unit * (S.t -> 'a t)
            | Set of S.t * (unit -> 'a t)
     >>>
  *)

  let get () = op (Get (fun x -> x))

  let set s = op (Set (s, fun x -> x))

  let run : type a. a t -> S.t -> a = fun m s ->
    let algebra : (a, S.t -> a) algebra =
      let return x = fun _ -> x in
      let op x s =
        let open Signature in
        match x with
        | Get f -> f s s
        | Set (s,f) -> f () s in
      {return; op}
    in run algebra m s
end
