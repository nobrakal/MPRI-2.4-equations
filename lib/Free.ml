open Functor

module Make (F : Functor) = struct
  module Base = struct
    type 'a t =
      | Return of 'a
      | Op of 'a t F.t

    let return a =
      Return a

    let rec bind m f =
      match m with
      | Return a ->
        f a
      | Op x ->
        Op (F.map (fun x -> bind x f) x)
  end

  module M = Monad.Expand (Base)
  include M
  open Base

  let op xs = Op (F.map return xs)

  type ('a, 'b) algebra =
    { return : 'a -> 'b
    ; op : 'b F.t -> 'b
    }

  let rec run alg m =
    match m with
    | Return x -> alg.return x
    | Op x     -> alg.op (F.map (run alg) x)
end
