module Signature = struct
  type 'a t =
    | Ok of 'a
    | Err of exn

  let map f s =
    match s with
    | Ok x -> Ok (f x)
    | Err e -> Err e
end

module Error = Free.Make (Signature)
include Error

let err e = op (Signature.Err e)

let run m =
  let alg =
    let return x = x in
    let op x =
      match x with
      | Signature.Ok x -> x
      | Signature.Err e -> raise e in
    {return; op}
  in
  Error.run alg m
