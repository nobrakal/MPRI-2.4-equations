module Make (Env : sig
  type t
end) =
struct
  module Base = struct
    type 'a t = Env.t -> 'a

    let return a = fun _ -> a

    let bind (m:'a t) (f:'a -> 'b t) : 'b t = fun env ->
      f (m env) env
  end

  module M = Monad.Expand (Base)
  include M

  let get () = fun x -> x

  let run m e = m e
end
